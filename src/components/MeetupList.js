import React from 'react';
import {
  View,
  ScrollView,
  Text,
  Image,
  TouchableWithoutFeedback,
  StyleSheet,
  Dimensions,
  ActivityIndicator
} from 'react-native';

import MeetupCard from './MeetupCard';
import { navigationOptions } from '../config/navOptions';
import { db } from '../config/firebase';

const { width, height } = Dimensions.get('window');

export default class MeetupList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      events: [],
      isLoading: true
    };
  }

  static navigationOptions = ({ navigation }) => ({
    title: 'Meetup',
    ...navigationOptions
  });

  componentWillMount() {
    db.ref('/events').once('value', snapshot => {
      this.setState({
        events: this.state.events.concat(snapshot.val()),
        isLoading: false
      });
    });
  }

  render() {
    const { navigation } = this.props;

    return this.state.isLoading ? (
      <ActivityIndicator style={styles.loader} size="large" color="#0000ff" />
    ) : (
      <ScrollView style={styles.container}>
        {this.state.events.map((event, i) => (
          <MeetupCard
            key={i}
            navigation={this.props.navigation}
            event={event}
          />
        ))}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  loader: {
    marginTop: 100
  },
  container: {
    flex: 1,
    margin: 10,
    height
  }
});
