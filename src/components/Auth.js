import React, { Component } from 'react';
import { TabNavigator } from 'react-navigation';

import Login from './Login';
import SignUp from './SignUp';
import { navigationOptions } from '../config/navOptions';

const Auth = TabNavigator(
  {
    Login: { screen: Login },
    SignUp: { screen: SignUp }
  },
  {
    tabBarOptions: {
      style: {
        backgroundColor: '#00376E',
        padding: 10
      },
      indicatorStyle: {
        backgroundColor: 'white',
        fontSize: 20,
        fontWeight: 'bold'
      }
    },
    navigationOptions: {
      ...navigationOptions
    }
  }
);

export default Auth;
