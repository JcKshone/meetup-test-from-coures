import React, { Component } from 'react';
import { View, Text, Button } from 'react-native';
import { auth } from './firebase';

export const navigationOptions = {
  headerRight: (
    <View style={{ flexDirection: 'row' }}>
      <Text style={{ paddingVertical: 7, color: 'white', paddingRight: 10 }}>
        {auth.currentUser ? auth.currentUser.email : ''}
      </Text>
      <Button color="gray" title="logout" onPress={() => auth.signOut()} />
    </View>
  ),
  headerStyle: {
    backgroundColor: '#00376E',
    paddingTop: 5,
    paddingHorizontal: 10,
    height: 30
  },
  headerTitleStyle: { color: 'white' },
  headerTintColor: 'white'
};
