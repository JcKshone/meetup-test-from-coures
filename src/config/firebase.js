import * as firebase from 'firebase';
export const firebaseApp = firebase.initializeApp({
  apiKey: 'AIzaSyDhKJEyiGJnstF39x6RZozzePZeuABeXvU',
  authDomain: 'meetuop-f7e00.firebaseapp.com',
  databaseURL: 'https://meetuop-f7e00.firebaseio.com',
  projectId: 'meetuop-f7e00',
  storageBucket: '',
  messagingSenderId: '280863156866'
});

export const db = firebaseApp.database();
export const auth = firebaseApp.auth();

export default firebase;
